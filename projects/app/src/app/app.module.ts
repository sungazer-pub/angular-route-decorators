import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InnerPageComponent } from './pages/inner-page/inner-page.component';
import { OuterPageComponent } from './pages/outer-page/outer-page.component';
import {AngularRouteDecoratorsModule} from '@sungazer/angular-route-decorators';
import { DeepPagePage } from './pages/deep-page/deep-page.page';
import { ContainerComponent } from './components/container/container.component';

@NgModule({
  declarations: [
    AppComponent,
    InnerPageComponent,
    OuterPageComponent,
    DeepPagePage,
    ContainerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularRouteDecoratorsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
