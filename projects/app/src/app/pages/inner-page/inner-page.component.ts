import {Component, OnDestroy, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {RouteParam, WithRouteDecorators, RouteQueryParam, RouteData} from '@sungazer/angular-route-decorators';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-inner-page',
  templateUrl: './inner-page.component.html',
  styleUrls: ['./inner-page.component.scss']
})
export class InnerPageComponent extends WithRouteDecorators() implements OnInit,OnDestroy {

  @RouteParam() innerId: string;
  @RouteParam({observable: true}) innerId$: Observable<string>;
  @RouteParam({paramName: 'innerId'}) innerIdD: string;
  @RouteParam({paramName: 'innerId', observable: true}) innerIdD$: Observable<string>;

  @RouteQueryParam() query: string;
  @RouteQueryParam({observable: true}) query$: Observable<string>;

  @RouteData() dataKey: string;
  @RouteData({observable: true}) dataKey$: Observable<string>;

  @RouteData() dataKeyObject: any;
  @RouteData({observable: true}) dataKeyObject$: Observable<any>;

  constructor(
    private location: Location
  ) {
    super();
  }

  ngOnInit(): void {
    super.ngOnInit();

    console.log('%cngOnInit!','font-weight: bolder');

    console.log('query', this.query);
    console.log('innerId', this.innerId);
    console.log('dataKey', this.dataKey);
    console.log('dataKeyObject', this.dataKeyObject);
    this.innerId$.subscribe(value => {
      console.log('innerId$: ', value);
    });
    this.query$.subscribe(value => {
      console.log('query$: ', value);
    });
    this.dataKey$.subscribe(value => {
      console.log('dataKey$',value);
    });
    this.dataKeyObject$.subscribe(value => {
      console.log('dataKeyObject$',value);
    });

  }

  back() {
    this.location.back();
  }
}
