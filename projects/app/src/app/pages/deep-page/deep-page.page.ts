import {Component, OnInit} from '@angular/core';
import {RouteParam, RouteQueryParam, WithRouteDecorators} from '@sungazer/angular-route-decorators';
import {Observable} from 'rxjs';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-deep-page',
  templateUrl: './deep-page.page.html',
  styleUrls: ['./deep-page.page.scss']
})
export class DeepPagePage extends WithRouteDecorators() implements OnInit {

  @RouteParam({observable: true}) bookId$: Observable<string>;


  constructor(private route: ActivatedRoute) {
    super();
  }

  ngOnInit(): void {
    console.log(this.route);
    super.ngOnInit();
  }

}
