import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeepPagePage } from './deep-page.page';

describe('DeepPagePage', () => {
  let component: DeepPagePage;
  let fixture: ComponentFixture<DeepPagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeepPagePage ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeepPagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
