import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {InnerPageComponent} from './pages/inner-page/inner-page.component';
import {OuterPageComponent} from './pages/outer-page/outer-page.component';
import {DeepPagePage} from './pages/deep-page/deep-page.page';
import {ContainerComponent} from './components/container/container.component';


const routes: Routes = [
  {
    path: '',
    component: OuterPageComponent
  },
  {
    path: 'inner/:innerId',
    component: InnerPageComponent
  },
  {
    path: 'inner-2/:innerId',
    component: InnerPageComponent,
    data: {
      dataKey: 'test',
      dataKeyObject: {a: 1, b: 2},
    }
  },
  {
    path: 'books/:bookId/new/free',
    component: DeepPagePage
  },
  {
    path: 'nested/books/:bookId/new',
    component: ContainerComponent,
    children: [
      {
        path: 'free',
        component: DeepPagePage
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {paramsInheritanceStrategy: 'always'})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
