import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo(): Promise<unknown> {
    return browser.get(browser.baseUrl) as Promise<unknown>;
  }

  getInnerLink(): Promise<string> {
    return element(by.css('app-root app-outer-page a')).getText() as Promise<string>;
  }
}
