import { AppPage } from './app.po';
import {browser, by, logging, element} from 'protractor';

describe('Inner Component', () => {

  beforeEach(() => {

  });

  it('inner/test-id', () => {
    browser.get(`${browser.baseUrl}/inner/test-id`);
    expect(element(by.id('param')).getText()).toEqual('Received innerId is test-id');
    expect(element(by.id('param$')).getText()).toEqual('Received innerId$ is test-id');
    expect(element(by.id('query')).getText()).toEqual('Received query is');
    expect(element(by.id('query$')).getText()).toEqual('Received query$ is');
    expect(element(by.id('dataKey')).getText()).toEqual('Received dataKey is');
    expect(element(by.id('dataKey$')).getText()).toEqual('Received dataKey$ is');
    expect(element(by.id('dataKeyObject')).getText()).toEqual('Received dataKeyObject is');
    expect(element(by.id('dataKeyObject$')).getText()).toEqual('Received dataKeyObject$ is');

    element(by.id('link-inner-query')).click();
    browser.waitForAngular();
    expect(element(by.id('param')).getText()).toEqual('Received innerId is test-id');
    expect(element(by.id('param$')).getText()).toEqual('Received innerId$ is test-id-2');
    expect(element(by.id('query')).getText()).toEqual('Received query is');
    expect(element(by.id('query$')).getText()).toEqual('Received query$ is test');
    expect(element(by.id('dataKey')).getText()).toEqual('Received dataKey is');
    expect(element(by.id('dataKey$')).getText()).toEqual('Received dataKey$ is');
    expect(element(by.id('dataKeyObject')).getText()).toEqual('Received dataKeyObject is');
    expect(element(by.id('dataKeyObject$')).getText()).toEqual('Received dataKeyObject$ is');

    browser.get(`${browser.baseUrl}/inner/test-id-2?query=test`);
    expect(element(by.id('param')).getText()).toEqual('Received innerId is test-id-2');
    expect(element(by.id('param$')).getText()).toEqual('Received innerId$ is test-id-2');
    expect(element(by.id('query')).getText()).toEqual('Received query is test');
    expect(element(by.id('query$')).getText()).toEqual('Received query$ is test');
    expect(element(by.id('dataKey')).getText()).toEqual('Received dataKey is');
    expect(element(by.id('dataKey$')).getText()).toEqual('Received dataKey$ is');
    expect(element(by.id('dataKeyObject')).getText()).toEqual('Received dataKeyObject is');
    expect(element(by.id('dataKeyObject$')).getText()).toEqual('Received dataKeyObject$ is');

    browser.get(`${browser.baseUrl}/inner-2/test?query=test`);
    expect(element(by.id('param')).getText()).toEqual('Received innerId is test');
    expect(element(by.id('param$')).getText()).toEqual('Received innerId$ is test');
    expect(element(by.id('query')).getText()).toEqual('Received query is test');
    expect(element(by.id('query$')).getText()).toEqual('Received query$ is test');
    expect(element(by.id('dataKey')).getText()).toEqual('Received dataKey is test');
    expect(element(by.id('dataKey$')).getText()).toEqual('Received dataKey$ is test');
    expect(element(by.id('dataKeyObject')).getText()).toEqual('Received dataKeyObject is { "a": 1, "b": 2 }');
    expect(element(by.id('dataKeyObject$')).getText()).toEqual('Received dataKeyObject$ is { "a": 1, "b": 2 }');
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
