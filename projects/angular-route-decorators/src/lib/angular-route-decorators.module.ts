import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {AngularRouteDecoratorsService} from './angular-route-decorators.service';


@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild([])
  ],
})
export class AngularRouteDecoratorsModule {
  // To trigger service instantiation
  constructor(service: AngularRouteDecoratorsService) {}
}
