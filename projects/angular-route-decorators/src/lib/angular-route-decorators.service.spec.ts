import { TestBed } from '@angular/core/testing';

import { AngularRouteDecoratorsService } from './angular-route-decorators.service';

describe('AngularRouteDecoratorsService', () => {
  let service: AngularRouteDecoratorsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AngularRouteDecoratorsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
