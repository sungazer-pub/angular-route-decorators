import { OnDestroy, OnInit, Directive } from '@angular/core';
import {AngularRouteDecoratorsService} from '../angular-route-decorators.service';
import {
  ROUTE_DATA_METADATA_KEY,
  ROUTE_PARAM_METADATA_KEY,
  ROUTE_QUERY_PARAM_METADATA_KEY, RouteParamMetadata
} from '../angular-route-decorators.metadata';
import {map} from 'rxjs/operators';
import {ActivatedRoute, ActivatedRouteSnapshot, ParamMap} from '@angular/router';
import {Observable} from 'rxjs';

type Constructor<T = {}> = new(...args: any[]) => T;

@Directive()
class BaseClass implements OnInit, OnDestroy {
  constructor(...args: any[]) {

  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
  }
}

export function WithRouteDecorators<T extends Constructor>(Base = BaseClass) {
  return class Temporary extends Base implements OnInit, OnDestroy {

    constructor(...args: any[]) {
      super(args);
    }

    ngOnInit() {
      if (super.ngOnInit) {
        super.ngOnInit();
      }

      processRouteDecorator(ROUTE_PARAM_METADATA_KEY, this, 'params');
      processRouteDecorator(ROUTE_QUERY_PARAM_METADATA_KEY, this, 'query');
      processRouteDecorator(ROUTE_DATA_METADATA_KEY, this, 'data');
    }

    ngOnDestroy() {
      if (super.ngOnDestroy) {
        super.ngOnDestroy();
      }
    }

  };
}

function processRouteDecorator(metadataKey: string | symbol, target: object, routeProperty: 'data' | 'params' | 'query') {
  // Get route from our service
  const srv = AngularRouteDecoratorsService.instance;
  const route = srv.getRouteForComponent(target.constructor as any);

  // Get meta keys
  const meta = Reflect.getOwnMetadata(metadataKey, target.constructor.prototype as any) as RouteParamMetadata[];
  if(!meta){
    return;
  }

  // Handle params
  for (const metaElem of meta) {
    if (metaElem.options.observable) {
      if(routeProperty === 'params' || routeProperty === 'query'){
        target[metaElem.propertyKey] = (routeProperty === 'params' ? route.paramMap : route.queryParamMap).pipe(
          map(value => value.get(metaElem.paramName))
        );
      } else if (routeProperty === 'data'){
        target[metaElem.propertyKey] = route.data.pipe(
          map(value => value[metaElem.paramName])
        );
      }
    } else {
      const snapshot = route.snapshot;
      if(routeProperty === 'params' || routeProperty === 'query'){
        target[metaElem.propertyKey] = (routeProperty === 'params' ? snapshot.paramMap : snapshot.queryParamMap).get(metaElem.paramName);
      } else if (routeProperty === 'data'){
        target[metaElem.propertyKey] = snapshot.data[metaElem.paramName];
      }
    }
  }
}
