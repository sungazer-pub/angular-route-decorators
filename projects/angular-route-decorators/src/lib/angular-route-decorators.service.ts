import {Injectable, Injector, Type} from '@angular/core';
import {filter, tap} from 'rxjs/operators';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {ROUTE_PARAM_METADATA_KEY} from './angular-route-decorators.metadata';

@Injectable({
  providedIn: 'root'
})
export class AngularRouteDecoratorsService {

  protected routeMap = new Map<Type<any>, ActivatedRoute>();

  constructor(private router: Router,
              private route: ActivatedRoute,
  ) {

    AngularRouteDecoratorsService.instance = this;

    this.router.events.pipe(
      filter(value => value instanceof NavigationEnd),
    ).subscribe((value: NavigationEnd) => {
      this.routeMap.clear();
      this.processRouterState(this.route.root);
    });
  }

  static instance: AngularRouteDecoratorsService = null;

  public getRouteForComponent(cmp: Type<any>): ActivatedRoute {
    return this.routeMap.get(cmp);
  }

  private processRouterState(route: ActivatedRoute) {
    const component = route.component as Type<any>;
    if (component && component.prototype) {
      const meta = Reflect.getOwnMetadata(ROUTE_PARAM_METADATA_KEY, component.prototype);
      if (meta) {
        this.routeMap.set(component, route);
      }
    }
    for (const child of route.children) {
      this.processRouterState(child);
    }
  }
}
