import 'reflect-metadata';
import {
  ROUTE_DATA_METADATA_KEY,
  ROUTE_PARAM_METADATA_KEY,
  ROUTE_QUERY_PARAM_METADATA_KEY,
  RouteParamMetadata
} from '../angular-route-decorators.metadata';

export interface RouteDecoratorOpts {
  paramName?: string;
  observable?: boolean;
}

export function RouteParam(opts: RouteDecoratorOpts = {paramName: null, observable: false}): PropertyDecorator {
  return (target, propertyKey) => {
    generateMetadata(ROUTE_PARAM_METADATA_KEY, target, propertyKey, opts);
  };
}

export function RouteQueryParam(opts: RouteDecoratorOpts = {paramName: null, observable: false}): PropertyDecorator {
  return (target, propertyKey) => {
    generateMetadata(ROUTE_QUERY_PARAM_METADATA_KEY, target, propertyKey, opts);
  };
}

export function RouteData(opts: RouteDecoratorOpts = {paramName: null, observable: false}): PropertyDecorator {
  return (target, propertyKey) => {
    generateMetadata(ROUTE_DATA_METADATA_KEY, target, propertyKey, opts);
  };
}

function generateMetadata(metadataKey: string | symbol, target: object, propertyKey: string | symbol, opts: RouteDecoratorOpts) {
  if (!opts.paramName) {
    opts.paramName = propertyKey.toString();
  }
  // Strip trailing $ to determine default param name
  if (opts.observable) {
    const index = opts.paramName.lastIndexOf('$');
    if (index === opts.paramName.length - 1) {
      opts.paramName = opts.paramName.slice(0, opts.paramName.length - 1);
    }
  }
  const meta = Reflect.getOwnMetadata(metadataKey, target) || [];
  const metaElem: RouteParamMetadata = {
    paramName: opts.paramName,
    propertyKey,
    options: {
      observable: opts.observable
    }
  }
  meta.push(metaElem);
  Reflect.defineMetadata(metadataKey, meta, target);
  // console.log('Defined metadata', metadataKey, meta);
}
