export const ROUTE_PARAM_METADATA_KEY = Symbol('RouteParam');
export const ROUTE_QUERY_PARAM_METADATA_KEY = Symbol('RouteQueryParam');
export const ROUTE_DATA_METADATA_KEY = Symbol('RouteData');

export interface RouteParamMetadata {
  paramName: string;
  propertyKey: string | symbol;
  options: {
    observable: boolean;
  }
}
