/*
 * Public API Surface of angular-route-decorators
 */

export * from './lib/angular-route-decorators.module';
export * from './lib/mixin/with-route-decorators.mixin';
export * from './lib/decorators/route-param';
export {RouteParamMetadata} from './lib/angular-route-decorators.metadata';
