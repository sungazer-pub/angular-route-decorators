# AngularRouteDecorators

This library offers 3 property decorators for common route operations. These decorators allow to automatically get snapshots or observables for values taken from the component's ActivatedRoute `paramMap`, `queryParamMap` and `data` properties.

## How to use

Import `AngularRouteDecoratorsModule` in your root AppModule.

Make sure that your component extends from the provided mixin class:

```ts
import {Component} from '@angular/core';
import {WithRouteDecorators} from '@sungazer/angular-route-decorators';

export class InnerPageComponent extends WithRouteDecorators() {

}
```

If your component is already extending a base component, just place the base component between the round brackets of the mixin.

```ts
import {Component} from '@angular/core';
import {WithRouteDecorators} from '@sungazer/angular-route-decorators';

export class InnerPageComponent extends WithRouteDecorators(BaseComponent) {

}
```

The mixin provides a base implementation for ngOnInit and ngOnDestroy, if your component uses any of these two hooks, please remember to call the parent method:

```ts
import {Component, OnDestroy, OnInit} from '@angular/core';
import {WithRouteDecorators} from '@sungazer/angular-route-decorators';

export class InnerPageComponent extends WithRouteDecorators() implements OnInit, OnDestroy {

  ngOnInit(){
    super.ngOnInit();
  }

  ngOnDestroy(){
    super.ngOnDestroy();
  }
}
```

Finally, decorate your properties

```ts

import {Component, OnDestroy, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {RouteParam, WithRouteDecorators, RouteQueryParam, RouteData} from '@sungazer/angular-route-decorators';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-inner-page',
  templateUrl: './inner-page.component.html',
  styleUrls: ['./inner-page.component.scss']
})
export class SampleComponent extends WithRouteDecorators() implements OnInit {

    // Contains the snapshot value for the path parameter 'param'
    @RouteParam() param: string;
    // Contains an observable that fires for any change of the path parameter 'param'
    @RouteParam({observable: true}) param$: Observable<string>;
    // Contains the snapshot value for the path parameter 'param'
    @RouteParam({paramName: 'param'}) param2: string;
    // Contains an observable that fires for any change of the path parameter 'param'
    @RouteParam({paramName: 'param', observable: true}) param2$: Observable<string>;
  
    // Contains the snapshot value for the query parameter 'query'
    @RouteQueryParam() query: string;
    // Contains an observable that fires for any change of the query parameter 'query'
    @RouteQueryParam({observable: true}) query$: Observable<string>;
    // (You can specify paramName here as well)
  
    // Contains the snapshot value for the data parameter 'dataKey'
    @RouteData() dataKey: string;
    // Contains an observable that fires for any change of the data parameter 'dataKey'
    @RouteData({observable: true}) dataKey$: Observable<string>;
    // (You can specify paramName here as well)
  
    // Same as above, the RouteData decorator also works for objects set in the data portion of the route
    @RouteData() dataKeyObject: any;
    @RouteData({observable: true}) dataKeyObject$: Observable<any>;

  constructor() {
    super();
  }

  ngOnInit(): void {
    super.ngOnInit();
  }
}
```

## Why the mixin class

According to https://github.com/angular/angular/issues/31495, it is not possible to runtime-patch functions such as ngOnInit etc. For the time being (waiting for Component Features ) the mixin helps in processing the decorator's metadata. 
